import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Home from '../screens/Home/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import Bag from '../screens/Bag';
const Tab = createMaterialBottomTabNavigator();
const Bottom = () => {
  return (
    <Tab.Navigator
      activeColor={'white'}
      inactiveColor="#112B3C"
      labeled={false}
      barStyle={{
        backgroundColor: 'red',
        justifyContent: 'center',
      }}
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen
        name="home"
        component={Home}
        options={{
          tabBarIcon: ({color}) => <Icon name="home" size={25} color={color} />,
        }}
      />
      <Tab.Screen
        name="bag"
        component={Bag}
        options={{
          tabBarIcon: ({color}) => (
            <Icon name="archive" size={25} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default Bottom;
