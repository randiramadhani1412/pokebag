import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {getPagination} from '../../../models/api';
const screen = Dimensions.get('screen');
const List = ({props}) => {
  const [data, setData] = useState([]);
  useEffect(() => {
    let offset = props.data - 1;
    getPagination(offset * 20, 20).then(dat => {
      setData(dat.results);
    });
  }, [props.data]);

  return (
    <>
      {data.map((dat, index) => {
        return (
          <TouchableOpacity
            style={styles.btn}
            key={index}
            onPress={() => {
              props.navigation.navigate('detail-pokemon', {
                link: dat.url,
                navigation: props.navigation.navigate,
              });
            }}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: 'white'}}>
              {dat.name}
            </Text>
          </TouchableOpacity>
        );
      })}
    </>
  );
};
const styles = StyleSheet.create({
  parent: {
    flex: 1,
    width: screen.width,
    height: screen.height,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    padding: 20,
  },
  btn: {
    backgroundColor: 'red',
    width: screen.width * 0.42,
    height: 55,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    margin: 5,
  },
});
export default List;
