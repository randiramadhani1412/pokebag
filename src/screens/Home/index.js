import React, {useState, useEffect, useCallback, useMemo} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getData} from '../../models/local';
import {getPagination} from '../../models/api';
const screen = Dimensions.get('screen');
import List from './components/list';
const Home = ({navigation}) => {
  const [data, setData] = useState(1);
  const [page, setPage] = useState(1);
  getData();
  const pagesCount = data => {
    if (page != null) {
      const countPage = Math.ceil(data / 20);
      return countPage;
    } else {
    }
  };
  const renderList = useMemo(() => {
    return <List props={{data: data, navigation: navigation}} />;
  }, [data]);

  useEffect(() => {
    getPagination(0, 20).then(dat => {
      setPage(pagesCount(dat.count));
    });
  }, [setPage]);
  return (
    <>
      <View style={styles.container}>
        <View style={styles.parent}>{renderList}</View>
        <SafeAreaView style={styles.scroll}>
          <ScrollView horizontal={true} style={styles.scrollView}>
            {[...Array(page)].map((x, i) => {
              const num = i + 1;
              return (
                <TouchableOpacity
                  key={i}
                  style={[
                    styles.page,
                    {backgroundColor: data == num ? 'red' : 'gray'},
                  ]}
                  onPress={() => {
                    setData(num);
                  }}>
                  <Text
                    style={{fontWeight: 'bold', color: 'white', fontSize: 18}}>
                    {num}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </SafeAreaView>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  parent: {
    flex: 1,
    width: screen.width,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    padding: 20,
  },
  btn: {
    width: screen.width * 0.42,
    height: 55,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',

    margin: 5,
  },
  pagination: {
    width: screen.width,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 20,
  },
  page: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginHorizontal: 5,
    backgroundColor: 'red',
    borderRadius: 10,
  },
  scroll: {
    width: screen.width * 0.5,
  },
  scrollView: {
    marginBottom: 50,
  },
});

export default Home;
