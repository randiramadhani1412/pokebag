import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import database from '@react-native-firebase/database';
import {Formik} from 'formik';
import {getRegister} from '../../models/user';
const screen = Dimensions.get('screen');
const Register = ({navigation}) => {
  return (
    <>
      <View style={styles.parent}>
        <Text style={{fontSize: 30, fontWeight: 'bold'}}>Register</Text>
        <Formik
          initialValues={{email: '', password: '', username: ''}}
          onSubmit={data => {
            getRegister.register(data, navigation);
          }}>
          {({handleChange, handleBlur, handleSubmit, values}) => (
            <View style={{width: '100%', alignItems: 'center'}}>
              <TextInput
                testID="username-input"
                style={styles.input}
                onChangeText={handleChange('username')}
                // value={email}
                placeholder="username"
                keyboardType="default"
              />
              <TextInput
                testID="email-input"
                style={styles.input}
                onChangeText={handleChange('email')}
                // value={email}
                placeholder="email"
                keyboardType="default"
              />
              <TextInput
                testID="password-input"
                style={styles.input}
                onChangeText={handleChange('password')}
                secureTextEntry={true}
                placeholder="Password"
                keyboardType="default"
              />
              <TouchableOpacity
                style={{
                  marginVertical: 10,
                  paddingVertical: 10,
                  paddingHorizontal: 30,
                  borderRadius: 50,
                  backgroundColor: 'red',
                }}
                onPress={handleSubmit}
                title="Submit">
                <Text
                  style={{fontSize: 18, color: 'white', fontWeight: 'bold'}}>
                  Register
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </Formik>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  parent: {
    flex: 1,
    height: screen.height,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    backgroundColor: 'gray',
    width: '70%',
    paddingHorizontal: 20,
    borderRadius: 50,
    marginVertical: 10,
    color: 'white',
  },
});
export default Register;
