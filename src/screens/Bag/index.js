import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import database from '@react-native-firebase/database';
import {getData} from '../../models/local';
const screen = Dimensions.get('screen');

const Bag = ({navigation}) => {
  const [data, setData] = useState([]);
  const [local, setLocal] = useState([]);

  const deletePokemon = async pokemonName => {
    await database().ref(`/users/${local.id}/pokemon/${pokemonName}`).remove();
    return true;
  };
  useEffect(() => {
    getData().then(value => {
      setLocal(value);
      database()
        .ref(`/users/${value.id}/pokemon/`)
        .on('value', snapshot => {
          snapshot.val() == null ? '' : setData(Object.values(snapshot.val()));
        });
    });
  }, [setData]);
  console.log(data);
  return (
    <>
      <View style={styles.container}>
        <View style={styles.parent}>
          {data.map((dat, index) => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderRadius: 10,
                  overflow: 'hidden',
                  marginVertical: 10,
                }}>
                <TouchableOpacity
                  style={styles.btn}
                  key={index}
                  onPress={() => {
                    navigation.navigate('detail-pokemon', {
                      link: dat.url,
                      navigation: navigation.navigate,
                    });
                  }}>
                  <Text
                    style={{fontSize: 18, fontWeight: 'bold', color: 'white'}}>
                    {dat.name}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    backgroundColor: 'blue',
                    padding: 5,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  key={index + 1}
                  onPress={() => {
                    deletePokemon(dat.name);
                  }}>
                  <Icon name="trash" size={25} color={'white'} />
                </TouchableOpacity>
              </View>
            );
          })}
        </View>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  parent: {
    flex: 1,
    width: screen.width,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  btn: {
    backgroundColor: 'red',
    width: screen.width * 0.42,
    height: 55,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Bag;
