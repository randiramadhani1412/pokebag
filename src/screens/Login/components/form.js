import React, {useState} from 'react';
import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Alert,
} from 'react-native';
import {Formik} from 'formik';
import {getLogin} from '../../../models/user';

const Form = ({props}) => {
  return (
    <>
      <Formik
        initialValues={{email: '', password: ''}}
        onSubmit={values => {
          getLogin.login(values, props, Alert.alert);
        }}>
        {({handleChange, handleBlur, handleSubmit, values}) => (
          <View style={{width: '100%', alignItems: 'center'}}>
            <TextInput
              testID="email-input"
              style={styles.input}
              onChangeText={handleChange('email')}
              // value={email}
              placeholder="email"
              keyboardType="default"
            />
            <TextInput
              testID="password-input"
              style={styles.input}
              onChangeText={handleChange('password')}
              secureTextEntry={true}
              placeholder="Password"
              keyboardType="default"
            />
            <TouchableOpacity
              style={{
                marginVertical: 10,
                paddingVertical: 10,
                paddingHorizontal: 30,
                borderRadius: 50,
                backgroundColor: 'red',
              }}
              onPress={handleSubmit}
              title="Submit">
              <Text style={{fontSize: 18, color: 'white', fontWeight: 'bold'}}>
                Login
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                props.navigate('register');
              }}>
              <Text>Register</Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
    </>
  );
};
const styles = StyleSheet.create({
  input: {
    backgroundColor: 'gray',
    width: '70%',
    paddingHorizontal: 20,
    borderRadius: 50,
    marginVertical: 10,
    color: 'white',
  },
});

export default Form;
