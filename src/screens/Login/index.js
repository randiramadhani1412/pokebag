import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
} from 'react-native';
import Form from './components/form';

const screen = Dimensions.get('screen');
const Login = ({navigation}) => {
  return (
    <View style={styles.parent}>
      <Image
        style={{width: screen.width * 0.4, height: screen.width * 0.4}}
        source={require('../../assets/logo.png')}
      />
      <Text style={{fontSize: 30, fontWeight: 'bold'}}>PokeBag</Text>
      <Form props={navigation} />
    </View>
  );
};
const styles = StyleSheet.create({
  parent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: screen.width,
    height: screen.height,
    backgroundColor: 'whiteSmoke',
  },
});
export default Login;
