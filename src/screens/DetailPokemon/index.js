import React, {useEffect, useState} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
} from 'react-native';
import database from '@react-native-firebase/database';
import AwesomeAlert from 'react-native-awesome-alerts';
// import {ALERT_TYPE, Dialog, Root, Toast} from 'react-native-alert-notification';
import {SvgCssUri} from 'react-native-svg';
import {getDetailPokemon} from '../../models/api';
import {getData} from '../../models/local';
const screen = Dimensions.get('screen');
const DetailPokemon = props => {
  const [data, setData] = useState({});
  const [local, setLocal] = useState({});
  const [show, setShow] = useState({
    con: false,
    title: '',
    msg: '',
    color: '',
  });

  const catchPokemon = () => {
    database()
      .ref(`/users/${local.id}/pokemon/${data.name}`)
      .once('value')
      .then(snapshot => {
        if (snapshot.val() == null) {
          if (Math.random() < 0.5) {
            database()
              .ref(`/users/${local.id}/pokemon/${data.name}`)
              .set({
                name: data.name,
                url: props.route.params.link,
              })
              .then(() =>
                setShow({
                  con: true,
                  title: 'Success!',
                  msg: 'Pokemon has been caught',
                  color: '#198754',
                }),
              );
          } else {
            setShow({
              con: true,
              title: 'Failed!',
              msg: 'pokemon not caught',
              color: '#f23030',
            });
          }
        } else {
          setShow({
            con: true,
            title: 'Warning!',
            msg: 'Pokemon already in the bag',
            color: '#ecbf58',
          });
        }
      });
  };
  useEffect(() => {
    getDetailPokemon(props.route.params.link).then(e => {
      setData(e);
    });
  }, [setData]);
  useEffect(() => {
    getData().then(value => {
      setLocal(value);
    });
  }, [setLocal]);
  return (
    <>
      <View style={styles.parent}>
        <View
          style={{
            alignItems: 'center',
            width: screen.width,
          }}>
          <SvgCssUri
            width="50%"
            height="50%"
            uri={
              data.sprites == undefined
                ? ''
                : data.sprites.other.dream_world.front_default
            }
          />
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: 'black',
              marginVertical: 5,
            }}>
            {data.name == undefined ? '' : data.name}
          </Text>
          <Text>
            {data.height == undefined
              ? ''
              : `H: ${data.height}  W: ${data.weight}  S:${data.species.name}`}
          </Text>
          <TouchableOpacity
            style={{
              paddingHorizontal: 20,
              paddingVertical: 5,
              backgroundColor: 'red',
              marginVertical: 5,
              borderRadius: 50,
            }}
            onPress={() => {
              catchPokemon();
            }}>
            <Text style={{color: 'white', fontWeight: 'bold'}}>Catch</Text>
          </TouchableOpacity>
          <AwesomeAlert
            show={show.con}
            showProgress={false}
            title={show.title}
            message={show.msg}
            closeOnTouchOutside={true}
            showConfirmButton={true}
            confirmText="OK"
            confirmButtonColor={show.color}
            onConfirmPressed={() => {
              setShow({con: false});
            }}
          />
          <View
            style={{
              width: screen.width,
              alignItems: 'flex-start',
              paddingHorizontal: 20,
            }}>
            <Text style={{fontSize: 30, color: 'black', fontWeight: 'bold'}}>
              Type
            </Text>
            <Text style={{fontSize: 18}}>
              {data.types == undefined
                ? ''
                : data.types.map((dat, index) => {
                    return `${dat.type.name}, `;
                  })}
            </Text>
            <Text
              style={{
                fontSize: 30,
                color: 'black',
                fontWeight: 'bold',
                marginTop: 10,
              }}>
              Ability
            </Text>
            <Text style={{fontSize: 18}}>
              {data.abilities == undefined
                ? ''
                : data.abilities.map((dat, index) => {
                    return `${dat.ability.name}, `;
                  })}
            </Text>
          </View>
          <TouchableOpacity
            style={{
              paddingHorizontal: 20,
              paddingVertical: 5,
              backgroundColor: 'red',
              marginTop: 30,
              borderRadius: 50,
            }}
            onPress={() => {
              console.log(props.route.params);
              props.route.params.navigation('bottom');
            }}>
            <Text style={{color: 'white', fontWeight: 'bold'}}>Back</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  parent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: screen.width,
    height: screen.height,
  },
});
export default DetailPokemon;
