import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Register from './screens/Register';
import Login from './screens/Login';
import Bottom from './utils/bottom';
import DetailPokemon from './screens/DetailPokemon';
const Stack = createNativeStackNavigator();
const Main = ({navigation}) => {
  return (
    <NavigationContainer style={{backgroundColor: '#131313'}}>
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#131313',
          justifyContent: 'center',
        }}>
        <StatusBar
          animated={true}
          barStyle="light-content"
          backgroundColor="transparent"
          showHideTransition={'fade'}
          translucent={true}
        />
        <Stack.Navigator
          initialRouteName="login"
          screenOptions={{headerShown: false}}>
          <Stack.Screen
            name="register"
            component={Register}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="detail-pokemon"
            component={DetailPokemon}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="login"
            component={Login}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="bottom"
            component={Bottom}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </SafeAreaView>
    </NavigationContainer>
  );
};

export default Main;
