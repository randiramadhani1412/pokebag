import database from '@react-native-firebase/database';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {v4 as uuidv4} from 'uuid';
const getLogin = {
  login(data, navigation, alert) {
    return database()
      .ref('/users/')
      .once('value')
      .then(snapshot => {
        const datas = Object.values(snapshot.val());
        const result = datas.filter(dat => {
          return dat.password === data.password && dat.email === data.email;
        });

        if (result.length > 0) {
          const datas = {
            id: result[0].id,
            login: true,
          };
          console.log(datas);
          const storeData = async value => {
            try {
              const jsonValue = JSON.stringify(value);
              await AsyncStorage.setItem('user', jsonValue);
              navigation.navigate('bottom');
            } catch (e) {
              console.log(e);
            }
          };
          storeData(datas);
        } else {
          alert('Login Gagal!', 'Pastikan password dan email benar');
        }
      });
  },
};
const getRegister = {
  register(data, navigation) {
    const id = uuidv4();
    database()
      .ref(`/users/${id}`)
      .set({
        id: id,
        username: data.username,
        email: data.email,
        password: data.password,
      })
      .then(() => {
        navigation.navigate('login');
      });
  },
};
export {getLogin, getRegister};
