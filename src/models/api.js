import axios from 'axios';
const getPagination = async (offset, limit) => {
  try {
    const response = await axios.get(
      `https://pokeapi.co/api/v2/pokemon/?offset=${offset}&limit=${limit}`,
    );
    return response.data;
  } catch (error) {}
};
const getDetailPokemon = async link => {
  try {
    const response = await axios.get(link);
    return response.data;
  } catch (error) {}
};

export {getPagination, getDetailPokemon};
